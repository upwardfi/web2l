window.upwardIncomeAppConfig = {
  webToLeadSubmitURL: 'https://dev5ig-upward-income.cs201.force.com/services/apexrest/newlead',
  webToLeadDefaultPartnerId: '00D0q0000009Z4Q',
  partnerOnboardingSubmitUrl: 'https://dev5ig-upward-income.cs201.force.com/services/apexrest/partner/onboarding',
  webToLeadUiURL: 'https://dev5ig-upward-income.cs201.force.com/#/web-to-lead-form/',
  enablePresetData: false
};
