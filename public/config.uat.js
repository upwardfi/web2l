window.upwardIncomeAppConfig = {
  webToLeadSubmitURL: 'https://uat-upward-income.cs17.force.com/services/apexrest/newlead',
  webToLeadDefaultPartnerId: '00D0q0000009Z4Q',
  partnerOnboardingSubmitUrl: 'https://uat-upward-income.cs17.force.com/services/apexrest/partner/onboarding',
  webToLeadUiURL: 'https://uat-upward-income.cs17.force.com/#/web-to-lead-form/',
  partnersProductsUrl: 'https://uat-upward-income.cs17.force.com/services/apexrest/products?partner_id=',
  saveCustomerProfileDataURL: 'https://uat-upward-income.cs17.force.com/services/apexrest/profiles-data',
  validateCustomerProfileDataURL: 'https://uat-upward-income.cs17.force.com/services/apexrest/profiles-data?contactId=',
  enablePresetData: false
};
