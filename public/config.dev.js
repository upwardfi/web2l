window.upwardIncomeAppConfig = {
  webToLeadSubmitURL: 'https://dev2-webtolead.cs66.force.com/services/apexrest/newlead',
  webToLeadDefaultPartnerId: '00D0q0000009Z4Q',
  partnerOnboardingSubmitUrl: 'https://dev2-webtolead.cs66.force.com/services/apexrest/partner/onboarding',
  webToLeadUiURL: 'https://dev1-upward.cs64.force.com/#/web-to-lead-form/',
  enablePresetData: true
};
