# [Upward - Web 2 Lead Form]() [![version][version-badge]][CHANGELOG] [![license][license-badge]][LICENSE]

> Upward Web 2 Lead Form based on light bootstrap dashboard UI template + vue-router

Includes Bootstrap 4, vue-router, chartist, google-maps and several other plugins/components.

## Getting started

Upward Web 2 Lead Form is built on top of Bootstrap 4, Vuejs and Vue-router. To get started do the following steps:
1. Download the project
2. Make sure you have node.js (https://nodejs.org/en/) installed
3. Type `npm install` in the source folder where `package.json` is located
4. Type `npm run dev` to start the development server
5. Go to [Web 2 Lead Form - http://localhost:8080/#/web-to-lead-form](http://localhost:8080/#/web-to-lead-form)

The repo uses [vue-cli](https://github.com/vuejs/vue-cli) scaffolding which takes care of the development setup with webpack and all the necessary modern tools to make web development faster and easier.

## Build Setup

### install dependencies
`npm install`
### serve with hot reload at localhost:8080
`npm run dev`
### build for production with minification
`npm run build`
### run unit tests
`npm run unit`
### run and watch unit tests
`npm run unit:watch`

## Contribution guide
* `npm install` or `yarn install`
* Please don't use jQuery or jQuery based plugins since there are many pure Vue alternatives

[CHANGELOG]: ./CHANGELOG.md
[LICENSE]: ./LICENSE.md
[version-badge]: https://img.shields.io/badge/version-0.0.1-blue.svg
[license-badge]: https://img.shields.io/badge/license-Proprietary-blue.svg
