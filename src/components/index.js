import BaseCheckbox from './Inputs/BaseCheckbox.vue'
import Radio from './Inputs/BaseRadio.vue'
import BaseInput from './Inputs/BaseInput.vue'
import BaseDropdownInput from './Inputs/BaseDropdownInput.vue'

import BaseDropdown from './BaseDropdown.vue'
import Table from './Table.vue'

let components = {
  BaseCheckbox,
  Radio,
  BaseInput,
  Table,
  BaseDropdown,
  BaseDropdownInput
}

export default components
