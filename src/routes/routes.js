import NotFound from '../pages/NotFoundPage.vue'
import WebToLeadPage from 'src/pages/WebToLeadPage.vue'
import PartnerOnboardingPage from 'src/pages/PartnerOnboardingPage.vue'
import CustomerInfoReviewPage from 'src/pages/CustomerInfoReviewPage.vue'

const routes = [
  {
    path: '/web-to-lead-form/:partnerId/:embed?/:consumer?',
    name: 'Web 2 Lead Form',
    component: WebToLeadPage,
    meta: {
      title: 'Upward - Web to Lead Form'
    }
  },
  {
    path: '/customer-info-review/:partnerId/:contactId/:token',
    name: 'Customer Info Review',
    component: WebToLeadPage,
    meta: {
      title: 'Upward - Customer Info Review'
    }
  },
  {
    path: '/partner-onboarding/:embed?/:partner?',
    name: 'Partner Onboarding',
    component: PartnerOnboardingPage,
    meta: {
      title: 'Upward - Partner Onboarding'
    }
  },
  { path: '*', component: NotFound }
];

/**
 * Asynchronously load view (Webpack Lazy loading compatible)
 * The specified component must be inside the Views folder
 * @param  {string} name  the filename (basename) of the view to load.
function view(name) {
   var res= require('../components/Dashboard/Views/' + name + '.vue');
   return res;
};**/

export default routes
